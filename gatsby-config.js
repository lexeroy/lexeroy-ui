require(`dotenv`).config({
  path: `.env`,
});

module.exports = {
  siteMetadata: {
    siteTitle: `lexeroy`,
    siteTitleAlt: `Allan Roy Balderama - Profile`,
    siteHeadline: `Allan Roy Balderama`,
    siteDescription: `Full-Stack Web Developer • JavaScript Junkie • Loves Front-End stuff, Adventure, Sunset 🌅 & Pizza 🍕`,
    siteLanguage: `en`,
    siteUrl: `https://allanroy.balderama.net`,
    siteImage: `/roybalderama.jpg`,
    author: `@roybalderama`,
    description: `Allan Roy Balderama - Portfolio`,
  },
  plugins: [
    {
      resolve: `@lekoarts/gatsby-theme-cara`,
      options: {},
    },
    {
      resolve: `gatsby-plugin-postcss`,
      options: {
        postCssPlugins: [require("autoprefixer")],
      },
    },
    `gatsby-plugin-sass`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: `UA-150082729-1`,
        head: false,
        pageTransitionDelay: 0,
        optimizeId: `GTM-MHS58X3`,
      },
    },
    {
      resolve: `gatsby-plugin-google-fonts`,
      options: {
        fonts: [`Roboto Mono\:300,400,400i,700`, "material icons"],
        display: "swap",
      },
    },
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Allan Roy Balderama - Full-Stack Web Developer`,
        short_name: `roybalderama`,
        description: `Allan Roy Balderama - Portfolio`,
        start_url: `/`,
        background_color: `#141821`,
        theme_color: `#f6ad55`,
        display: `standalone`,
        icons: [
          {
            src: `/android-icon-192x192.png`,
            sizes: `192x192`,
            type: `image/png`,
          },
        ],
      },
    },
    `gatsby-plugin-offline`,
    `gatsby-plugin-netlify`,
  ],
};

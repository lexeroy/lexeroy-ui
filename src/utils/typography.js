import Typography from 'typography';

const typography = new Typography({
  bodyFontFamily: ['Roboto Mono', 'serif'],
});

export default typography;

[![Netlify Status](https://api.netlify.com/api/v1/badges/c48a1b12-256e-497a-bc3d-c350007f0c88/deploy-status)](https://app.netlify.com/sites/lexeroy/deploys)

<p align="center">
  <a href="https://lexeroy.xyz/">
    <img alt="LekoArts" src="https://lexeroy.xyz/android-icon-192x192.png" />
  </a>
</p>
<h1 align="center">
  Allan Roy Balderama - Portfolio Website
</h1>



This gatsbyjs project is a customized fork from lekoArts' [gatsby-starter-portfolio-cara](https://github.com/LekoArts/gatsby-starter-portfolio-cara/) project. You can check there how to setup.

To use a starter-theme, you need to understand how to shadow a react theme -> you can check this [instruction](https://www.gatsbyjs.org/docs/themes/shadowing/) to learn how to do it.